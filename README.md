# karAPPllot

![](others/intro.png)

Processes the output of a MassSpec and, for a given peptide, retrieves the subcellular location (from UniProt) and the location of that specific peptide in the reference sequence.

## Usage

The app has to be called within the [app](app/) directory.

In Mac/Linux:

```
$ python app.py
```

In Windows cmd:

```
 py app.py
```

A window like this will show up:

![](others/main.png)

1. Click on the `Input file (.xlsx)` button, browse the input excel file (a sample of these is found in [TEST.xlsx](TEST.xlsx) and [data.xlsx](data.xlsx)).
2. Check that the Sequence and Protein Accession columns are well identified.
3. The output file will be automatically determined.
4. `Dale` and wait until it finishes.

##  How does it work

The script basically takes the Uniprot Accession for each peptide (from where the peptide is supposed to come) and parses the Uniprot entry to retrieve both **GO Subcellular location** and **Canonical Sequence**.

With the **Subcellular location**, it transforms the GO terms to a set of 0-9 numbers according to:

```
terms = {1: ['Extracellular region', 'Secreted'],
         2: ['Endosome', 'Lysosome'],
         3: ['Plasma membrane', 'Membrane', 'Cell membrane'],
         4: ['Golgi apparatus'],
         5: ['Endoplasmic reticulum', 'Ribosomal'],
         6: ['Mitochondrion'],
         7: ['Cytosol', 'Cytoplasm', 'Cytoskeleton'],
         8: ['Nucleus'],
         9: ['Other locations'],
         0: ['Undefined']}
```

With the **Canonical Sequence**, tries to find where the specific peptide is located: (a) C-ter, (b) Central or (c) N-ter.

This information is added in two new columns in the ***out.xlsx*** file.

## Distribution

In the [app](app/) folder, all the files needed for the app to work are found:
- `app.py` contains the main code for the app.
- `functions.py` contains the functions used in `app.py`
- `principal.ui` contains the design of the GUI
- `repo.xlsx` is a local repository of Uniprot data, to avoid re-accessing the webpage if an accession has been already searched

In the directory [quim](quim/) an improved version of the app is found, but under development.

## Requirements

```
- python3 or higher
- pandas
- openpyxl
- bs4
- PyQt4 or higher
- xlrd
- xlsxwriter
```

## Installation

In a **windows OS**, from scratch:

1. [Download python](https://www.python.org/ftp/python/3.7.1/python-3.7.1.exe).
2. Download pip:
  - Download [this file](https://bootstrap.pypa.io/get-pip.py).
  - On the cmd: `py get-pip.py`
3. Download the packages (one-by-one) with: `py -m pip install {package}`
4. PyQt4 won't probably install, so the best way to do it is like it's done [here](https://www.youtube.com/watch?v=SRtBv_kSsak).
