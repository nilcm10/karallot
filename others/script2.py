#!/usr/bin/env python3

# Reading input

import sys, re, ssl, datetime

h = '''
    The right usage of the script:
        $ python3 script2.py <file_name>
    The file name has to be included, but WITHOUT the extension!
    The script returns a <file_name>_out2.xlsx as output.

    You can also do the following (if using Mac/Linux OS):
        $ chmod +x karallot.py
        $ ./karallot.py <file_name>

    You need python 3 installed in your computer!!!
    The python packages you need for this script to work are:
    - pandas
    - urllib
    - bs4
    You can install them by simply:
        $ python3 -m pip install <package-name>
    '''

if len(sys.argv) < 2:
    print('ERROR: No file provided.')
    print(h)
    sys.exit()

if '-h' in sys.argv:
    print(h)
    sys.exit()

fin_name = str(sys.argv[1])
t0 = datetime.datetime.now()


# Importing packages

try:
    import pandas as pd
except:
    print('''
    Could not import pandas module. To install it:
        $ python3 -m pip install pandas
    For further information, check https://pypi.org/project/pandas/''')
try:
    import urllib.request
except:
    print('''
    Could not import urllib module. To install it:
        $ python3 -m pip install urllib
    For further information, check
    https://docs.python.org/3/library/urllib.request.html#module-urllib.request
    ''')


# Ignore SSL certificate errors

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


# Reading xlsx file

fin = pd.ExcelFile(fin_name+'.xlsx')
sheet = fin.parse()


# Obtaining relevant information from file

accs = []
for acc in sheet['Protein Group Accessions']:
	if ';' in acc:
		cut = acc.index(';')
		acc = acc[:cut]
		accs.append(str(acc))
	else:
		accs.append(str(acc))

peps = []
for pep in sheet['Sequence']:
	peps.append(str(pep))


# Functions

def fetch_fasta():
	''' 
	Obtains the fasta file corresponnding to the accession number given (UniProt ID). 
	
	Expected output:
		List with fasta links corresponding to the UniProtIDs given in the input file.
	'''

	URL = 'http://www.uniprot.org/uniprot/'
	urls = []
	for a in accs:
		urls.append(URL+str(a)+'.fasta')
	return(urls)

def prot_sequence(URL):
	''' Retrieves the protein sequence from the fasta file provided. 

	Input:
		Fasta link.
	Expected output:
		Protein sequence (str)
	'''

	u = urllib.request.urlopen(URL, context=ctx).read()
	uBS = u.decode('utf-8').split('\n')
	prots = []
	for l in uBS:
		w = re.findall('^(\w.*)',l)
		if len(w) > 0:
			prots.append(w[0])
	prots = ''.join(prots)
	return(prots)

def sequence_location(peptide, protein):
	''' Determine the region of the protein sequence where the peptide matches.

	Input:
		Peptide and protein sequences (str)
	Expected output:
		Region identifier (str)
	'''

	for i in range(0, len(protein)-len(peptide)+1):
		if peptide == protein[i:i+len(peptide)]:
			if i <= 19:
				return('N-terminal')
			if i >= (len(protein)-39):
				return('C-terminal')
			else:
				return('Central')


def output_generator():
	''' Writes output into a new column of the xslx file.

	Expected output:
		List of region identifiers corresponding to the UniProtIDs in the input file,
		in the same order.
	'''

	links = fetch_fasta()
	seqs = []
	for link in links:
		p = prot_sequence(link)
		seqs.append(p)
		location = []
	for j in range(0,len(peps)):
		location.append(sequence_location(peps[j], seqs[j]))
	return(location)

final_location = output_generator()


# Appending the new column to sheet

loc_col = [str(x) for x in final_location]
sheet['Protein sequence matching'] = pd.Series(loc_col, index=sheet.index)


# Creating output xlsx file

fout_name = fin_name+'_out2'
fout = pd.ExcelWriter(fout_name+'.xlsx', engine='xlsxwriter')
sheet.to_excel(fout,'Sheet1')
fout.save()

# Completion message


t = datetime.datetime.now() - t0
print('''
    --- Job finished correctly ---

    Took {} h:min:s

    Check the {}.xlsx
'''.format(str(t), fout_name))
