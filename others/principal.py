'''
KARALLOT.PY -

@authors: quim, nil, claudia
'''

## Imports

# General imports
import sys
import re
import ssl
import datetime
import operator

# Specific imports
try:
    import pandas as pd
except:
    print('''
    Could not import pandas module. To install it:
        $ python3 -m pip install pandas
    For further information, check https://pypi.org/project/pandas/''')
try:
    import urllib.request, urllib.error
except:
    print('''
    Could not import urllib module. To install it:
        $ python3 -m pip install urllib
    For further information, check
    https://docs.python.org/3/library/urllib.request.html#module-urllib.request
    ''')
try:
    from bs4 import BeautifulSoup
except:
    print('''
    Could not import bs4 module. To install it:
        $ python3 -m pip install bs4
    For further information, check https://pypi.org/project/bs4/''')

try:
    from openpyxl import load_workbook
except:
    print('''
    Could not import openpyxl module. To install it:
        $ python3 -m pip install openpyxl
    For further information, check https://pypi.org/project/openpyxl/''')
try:
    from PyQt4 import QtGui, uic
except:
    print('''
    Could not import PyQt4 module.
    For further information, check https://pypi.org/project/PyQt4/''')

## Application

# UI Design input
qtCreatorFile = "principal.ui" # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

# Class definition
class MyApp(QtGui.QMainWindow, Ui_MainWindow):

    fin_name = ""
    fout_name = ""

    def __init__(self):
        '''
        Standard initialization of QtGUI
        '''

        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.entryButton.clicked.connect(self.open_file)
        self.fileOut.textChanged.connect(self.change_fout)
        self.launchButton.clicked.connect(self.process)
        self.despSequence.activated.connect(self.refresh_columns)
        self.despPGAcc.activated.connect(self.refresh_columns)


    def refresh_columns(self):
        '''
        Check if columns sequence and protein accessions are present
        '''

        self.sequence_column = self.despSequence.currentText()
        self.pgacc_column = self.despPGAcc.currentText()
        self.despSequence.setEnabled(True)
        self.despPGAcc.setEnabled(True)
        if self.sequence_column != "" and self.pgacc_column != "":
            self.launchButton.setEnabled(True)
        else:
            self.launchButton.setEnabled(False)


    def change_fout(self):
        '''
        Setting file out name
        '''

        self.fout_name = self.fileOut.text() + ".xlsx"

    sequence_column = "Sequence"
    pgacc_column = "Protein Group Accessions"


    def fill_columns(self):
        '''
        Once file is opened, sequence and p.acc desplegables are filled
        '''

        sheet = pd.ExcelFile(self.fin_name).parse()
        columns = sheet.columns.get_values()
        self.despSequence.clear()
        self.despPGAcc.clear()
        self.despSequence.addItem("")
        self.despPGAcc.addItem("")
        i = 1
        seqCol = 0
        pgaCol = 0
        for column in columns:
            self.despSequence.addItem(column)
            if self.sequence_column == column:
                seqCol = i
            self.despPGAcc.addItem(column)
            if self.pgacc_column == column:
                pgaCol = i
            i = i+1
        self.despSequence.setCurrentIndex(seqCol)
        self.despPGAcc.setCurrentIndex(pgaCol)
        self.sequence_column = self.despSequence.currentText()
        self.pgacc_column = self.despPGAcc.currentText()
        self.refresh_columns()


    def open_file(self):
        '''
        File browse and parsing through pandas Excel file handling
        '''

        self.fin_name = QtGui.QFileDialog.getOpenFileName(self, 'Choose file',
                                                            '(*.xlsx)') #filter
        self.performance.setText("")
        self.logCheck.setText("")

        self.fout_name = self.fin_name[:-5] + '_out.xlsx'
        self.label_fin.setText(self.fin_name)
        self.fileOut.setText(self.fin_name[:-5] + '_out')
        self.fill_columns()


    def process(self):
        '''
        Main process
        '''

        # Performance start
        start = str(datetime.datetime.now())
        self.performance.setText("From " + start)
        LOG = "Initializing process...\n"
        self.logCheck.setText(LOG)
        sheet = pd.ExcelFile(self.fin_name).parse()

        # Core process
        loc_ids, loc_names, z = [],[],0
        for acc in sheet[self.pgacc_column]:
            if ';' in acc:
                cut = acc.index(';')
                acc = acc[:cut]
            pair = self.uid_to_loc(acc)

            z += 1

            self.progressCounter.setText('%s / %s \r' % (z, len(sheet)))
            progress = (z / len(sheet)) * 100
            self.progressBar.setValue(progress)
            QtGui.qApp.processEvents()

            loc_names.append(pair[1]) # location ID
            loc_ids.append(pair[2]) # location name

        sheet['Location ID'] = pd.Series(loc_ids, index=sheet.index)
        sheet['Location Names'] = pd.Series(loc_names, index=sheet.index)

        # Writing out excel file
        fout = pd.ExcelWriter(self.fout_name, engine='xlsxwriter')
        sheet.to_excel(fout,'Base')

        # Performance end
        end = str(datetime.datetime.now())
        self.performance.setText("From " + start + " to " + end)
        self.progressCounter.setText('%s / %s' % (z, len(sheet)))
        issue = '''
        #################
        ###  DONE!!!  ###
        #################

        Ets un tros de karallot!!!!!
        '''
        LOG = LOG + issue
        self.logCheck.setText(LOG)

    # Dictionary with the location names and ids correspondance
    terms = {1:['Extracellular region', 'Secreted'],
             2: ['Endosome', 'Lysosome'],
             3: ['Plasma membrane', 'Membrane', 'Cell membrane'],
             4: ['Golgi apparatus'],
             5: ['Endoplasmic reticulum', 'Ribosomal'],
             6: ['Mitochondrion'],
             7: ['Cytosol', 'Cytoplasm', 'Cytoskeleton'],
             8: ['Nucleus'],
             9: ['Other locations'],
             0: ['Undefined']}


    def loc_id(self, locs, uid):
        '''
        From a location name to a location ID. Looks for the correspondance in
        the dictionary terms, and returns the location ID.

        Input:
            List of location names
        Expected output:
            List of location IDs
        '''

        if locs[0][:5] != 'Error':
            lenlocs = len(locs)
            ids = [None]*lenlocs

            if lenlocs < 1:
                ids = [0]
                issue = '''
            >>> No subcellular location is defined for {}. Check GO Annotation.
            '''.format(uid)
                LOG = LOG + issue + '\n'
                self.logCheck.setText(LOG)
            else:
                for i in range(lenlocs):
                    for j in range(len(self.terms)):
                        if locs[i] in self.terms[j]:
                            ids[i] = j

            for i in range(len(ids)):
                if ids[i] == None:
                    ids[i] = 9

            ids = list(set(ids))
            ids.sort()
            return ids

    # Local repository
    sheetRepo = pd.ExcelFile('LocationIDRepo.xlsx').parse()
    repoUID = []
    repoLocID = []
    repo = []

    for i in sheetRepo['UID']:
        repoUID.append(i)

    for i in sheetRepo['LocID']:
        repoLocID.append(i)

    for i in range(len(repoUID)):
        repoRow= []
        repoRow.append(repoUID[i])
        repoRow.append(eval(repoLocID[i]))
        repo.append(repoRow)
    repo = sorted(repo, key=operator.itemgetter(0))
    print(repo)

    def uid_to_loc(self, uid):
        '''
        From a UniProt ID to a location. The function either checks if the
        Accession is in the local repository or parses the UniProt web entry to
        find the location.

        Input:
            UniProt ID (str)
        Expected output:
            List with the UniProt ID, the location name and ID.
        '''

        uidLoc = []
        hiEs = False
        # is it in the repo?
        for uidR in self.repo:
            hiEs = False
            if uidR[0] == uid:
                hiEs = True
                term = []
                for u in uidR[1]:
                    term.append(self.terms[u])
                uidLoc = [uidR[0],term,uidR[1]]
                break
        # or in the website?
        if hiEs == False:
            uidLoc = self.uid_to_loc_uniprot(uid)
            row_data = [uidLoc[0],uidLoc[2]]
            row_dataStr = [uidLoc[0],str(uidLoc[2])]
            if uidLoc[1][0][:5] != 'Error':
                self.repo.append(row_data)

                wb = load_workbook("LocationIDRepo.xlsx")
                ws = wb.worksheets[0]
                ws.append(row_dataStr)
                wb.save("LocationIDRepo.xlsx")
                self.repo = sorted(self.repo, key=operator.itemgetter(0))

        return uidLoc


    def uid_to_loc_uniprot(self, uid):
        '''
        Function to parse UniProt
        '''

        # to avoid SSL certificate errors (from @csev)
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        URL = 'https://www.uniprot.org/uniprot/' + uid
        locs = []
        try:
            u = urllib.request.urlopen(URL, context=ctx).read()
            uBS = BeautifulSoup(u, 'html.parser')
            str2='Keywords - Cellular component'
            uu=u.decode("utf-8")
            hies=uu.find(str2)
            if hies!=-1:
                s=uBS.find(id="subcellular_location")
                if s != []:
                    span=s('span')
                    for j in range(len(span)):
                        p = re.findall('.*(Cellular component).*', str(span[j]))
                        if p != []:
                            loc = span[j+1]
                            for l in loc('a'):
                                kw = re.findall('>(.*?)</a>', str(l))
                                if len(kw) > 0:
                                    locs.append(kw[0])

        except urllib.error.HTTPError as e:
            if e.code == 404:
                locs = ['Error 404: Not found']
                issue = ">>> {} - {}".format(uid, locs[0])
                LOG = LOG + issue + '\n'
                self.logCheck.setText(LOG)
            else:
                locs=["Error " + u.code + ': ']
                issue = ">>> {} - {}".format(uid, locs[0])
                LOG = LOG + issue + '\n'
                self.logCheck.setText(LOG)
        except urllib.error.URLError as e:
            locs=["Error :" + e.reason]
            issue = ">>> {} - {}".format(uid, locs[0])
            LOG = LOG + issue + '\n'
            self.logCheck.setText(LOG)

        if locs == []:
            locs=['Undefined']

        return (uid, locs, self.loc_id(locs, uid))

## Execution

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
