'''
KARALLOT App - Doing useful stuff since 2018

@authors: quim, nil, claudia
'''

## Importing functions

from functions import *

## Application

# UI Design input
qtCreatorFile = "principal.ui" # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

# MyAPP
class MyApp(QtGui.QMainWindow, Ui_MainWindow):

    fin_name = ""
    fout_name = ""

    def __init__(self):
        '''
        Standard initialization of QtGUI
        '''

        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.entryButton.clicked.connect(self.open_file)
        self.fileOut.textChanged.connect(self.change_fout)
        self.launchButton.clicked.connect(self.process)
        self.despSequence.activated.connect(self.refresh_columns)
        self.despPGAcc.activated.connect(self.refresh_columns)


    def refresh_columns(self):
        '''
        Check if columns sequence and protein accessions are present
        '''

        self.sequence_column = self.despSequence.currentText()
        self.pgacc_column = self.despPGAcc.currentText()
        self.despSequence.setEnabled(True)
        self.despPGAcc.setEnabled(True)
        if self.sequence_column != "" and self.pgacc_column != "":
            self.launchButton.setEnabled(True)
        else:
            self.launchButton.setEnabled(False)


    def change_fout(self):
        '''
        Setting file out name
        '''

        self.fout_name = self.fileOut.text() + ".xlsx"

    sequence_column = "Sequence"
    pgacc_column = "Protein Group Accessions"

    def fill_columns(self):
        '''
        Once file is opened, sequence and p.acc desplegables are filled
        '''

        sheet = pd.ExcelFile(self.fin_name).parse()
        columns = sheet.columns.get_values()
        self.despSequence.clear()
        self.despPGAcc.clear()
        self.despSequence.addItem("")
        self.despPGAcc.addItem("")
        i = 1
        seqCol = 0
        pgaCol = 0
        for column in columns:
            self.despSequence.addItem(column)
            if self.sequence_column == column:
                seqCol = i
            self.despPGAcc.addItem(column)
            if self.pgacc_column == column:
                pgaCol = i
            i = i+1
        self.despSequence.setCurrentIndex(seqCol)
        self.despPGAcc.setCurrentIndex(pgaCol)
        self.sequence_column = self.despSequence.currentText()
        self.pgacc_column = self.despPGAcc.currentText()
        self.refresh_columns()


    def open_file(self):
        '''
        File browse and parsing through pandas Excel file handling
        '''

        self.fin_name = QtGui.QFileDialog.getOpenFileName(self, 'Choose file',
                                                            './',
                                                            'Excel (*.xlsx)')
        self.performance.setText("")
        self.logCheck.setText("")

        self.fout_name = self.fin_name[:-5] + '_out.xlsx'
        self.label_fin.setText(self.fin_name)
        self.fileOut.setText(self.fin_name[:-5] + '_out')
        self.fill_columns()


    # Local repository
    repo = pd.ExcelFile('repo.xlsx').parse()

    def process(self):
        '''
        Main process
        '''

        # Performance start
        start = datetime.datetime.now()
        self.performance.setText("From {}".format(str(start)))
        LOG = "Initializing process...\n"
        self.logCheck.setText(LOG)
        sheet = pd.ExcelFile(self.fin_name).parse()

        # Core process 1 - subcellular location
        loc_ids, matches, z = [], [], 0
        for acc in sheet[self.pgacc_column]:
            if ';' in acc:
                cut = acc.index(';')
                acc = acc[:cut]

            # is it in the repo?
            if acc in self.repo.UID.get_values():
                array = self.repo[self.repo.UID == acc].get_values()
                result = array.tolist()[0]

            # or in the website?
            else:
                uid_loc = UidToLoc.uniprot_parse(UidToLoc, acc)
                uid_seq = PeptidePlacer.prot_sequence(acc)
                result = [uid_loc[0], str(uid_loc[2]), uid_seq]

                if uid_loc[1][0][:5] != 'Error':
                    self.repo = self.repo.append(pd.DataFrame([result],
                                                    columns=self.repo.columns),
                                                    ignore_index=True)
                    frepo = pd.ExcelWriter('repo.xlsx', engine='xlsxwriter')
                    self.repo.to_excel(frepo, 'Repo')
                    frepo.save()

            # Core process 2 - peptide location in sequence
            refseq = sheet[self.sequence_column].iloc[z]
            match = PeptidePlacer.sequence_location(refseq, result[2])
            loc_ids.append(result[1])
            matches.append(match)

            z += 1
            self.progressCounter.setText('%s / %s \r' % (z, len(sheet)))
            progress = (z / len(sheet)) * 100
            self.progressBar.setValue(progress)
            QtGui.qApp.processEvents()

        sheet['Location ID'] = pd.Series(loc_ids, index=sheet.index)
        sheet['Peptide Match'] = pd.Series(matches, index=sheet.index)

        # Writing out excel file
        fout = pd.ExcelWriter(self.fout_name, engine='xlsxwriter')
        sheet.to_excel(fout, 'Sheet1')
        fout.save()

        # Performance end
        end = datetime.datetime.now()
        delta = end - start
        text = "From {} to {}. Took {} h:min:s".format(str(start),
                                                        str(end),
                                                        str(delta))
        self.performance.setText(text)
        self.progressCounter.setText('%s / %s' % (z, len(sheet)))
        issue = '''
        #################
        ###  DONE!!!  ###
        #################

        Ets un tros de karallot!!!!!
        '''
        LOG = LOG + issue
        self.logCheck.setText(LOG)

## Execution
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
