'''
FUNCTIONS FOR KARALLOT App

@authors: nil, claudia
'''

## Imports

# General imports
import sys
import re
import ssl
import datetime

# Specific imports
try:
    import pandas as pd
except:
    print('''
    Could not import pandas module. To install it:
        $ python3 -m pip install pandas
    For further information, check https://pypi.org/project/pandas/''')
try:
    import urllib.request, urllib.error
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
except:
    print('''
    Could not import urllib module. To install it:
        $ python3 -m pip install urllib
    For further information, check
    https://docs.python.org/3/library/urllib.request.html#module-urllib.request
    ''')
try:
    from bs4 import BeautifulSoup
except:
    print('''
    Could not import bs4 module. To install it:
        $ python3 -m pip install bs4
    For further information, check https://pypi.org/project/bs4/''')

try:
    from openpyxl import load_workbook
except:
    print('''
    Could not import openpyxl module. To install it:
        $ python3 -m pip install openpyxl
    For further information, check https://pypi.org/project/openpyxl/''')
try:
    from PyQt4 import QtGui, uic
except:
    print('''
    Could not import PyQt4 module.
    For further information, check https://pypi.org/project/PyQt4/''')

## Functions

class UidToLoc:

    # Dictionary with the location names and ids correspondance
    terms = {1: ['Extracellular region', 'Secreted'],
             2: ['Endosome', 'Lysosome'],
             3: ['Plasma membrane', 'Membrane', 'Cell membrane'],
             4: ['Golgi apparatus'],
             5: ['Endoplasmic reticulum', 'Ribosomal'],
             6: ['Mitochondrion'],
             7: ['Cytosol', 'Cytoplasm', 'Cytoskeleton'],
             8: ['Nucleus'],
             9: ['Other locations'],
             0: ['Undefined']}

    def loc_id(self, locs, uid):
        '''
        From a location name to a location ID. Looks for the correspondance in
        the dictionary terms, and returns the location ID.

        Input:
            List of location names
        Expected output:
            List of location IDs
        '''

        if locs[0][:5] != 'Error':
            lenlocs = len(locs)
            ids = [None]*lenlocs

            if lenlocs < 1:
                ids = [0]
            else:
                for i in range(lenlocs):
                    for j in range(len(self.terms)):
                        if locs[i] in self.terms[j]:
                            ids[i] = j

            for i in range(len(ids)):
                if ids[i] == None:
                    ids[i] = 9

            ids = list(set(ids))
            ids.sort()

            return ids


    def uniprot_parse(self, uid):
        '''
        Function to get Subcellular location from UniProt ID parsing UniProt.

        Input:
            UniProt ID (str)

        Expected output:
            List with the UniProt ID (str), a list of location names and
            location IDs.
        '''

        # to avoid SSL certificate errors (from @csev)
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        URL = 'https://www.uniprot.org/uniprot/' + uid
        locs = []
        try:
            u = urllib.request.urlopen(URL, context=ctx).read()
            uBS = BeautifulSoup(u, 'html.parser')
            str2 = 'Keywords - Cellular component'
            uu = u.decode("utf-8")
            hies = uu.find(str2)
            if hies != -1:
                s = uBS.find(id="subcellular_location")
                if s != []:
                    span = s('span')
                    for j in range(len(span)):
                        p = re.findall('.*(Cellular component).*', str(span[j]))
                        if p != []:
                            loc = span[j+1]
                            for l in loc('a'):
                                kw = re.findall('>(.*?)</a>', str(l))
                                if len(kw) > 0:
                                    locs.append(kw[0])

        except urllib.error.HTTPError as e:
            if e.code == 404:
                locs = ['Error 404: Not found']
            else:
                locs = ["Error " + u.code + ': ']
        except urllib.error.URLError as e:
            locs = ["Error :" + e.reason]

        if locs == []:
            locs=['Undefined']

        return [uid, locs, self.loc_id(self, locs, uid)]


class PeptidePlacer:

    def prot_sequence(acc):
        ''' Retrieves the protein sequence from the fasta file provided.
                Input:
                    Fasta link.
                Expected output:
                    Protein sequence (str)
        '''

        URL = 'http://www.uniprot.org/uniprot/{}.fasta'.format(acc)
        u = urllib.request.urlopen(URL, context=ctx).read()
        uBS = u.decode('utf-8').split('\n')
        prots = []
        for l in uBS:
            w = re.findall('^(\w.*)',l)
            if len(w) > 0:
                prots.append(w[0])
        prots = ''.join(prots)

        if len(prots) == 0:
            prots = 'No sequence available'
        return(prots)


    def sequence_location(peptide, protein):
        ''' Determine the region of the protein sequence where the peptide
            matches.

        	Input:
             Peptide and protein sequences (str)
           Expected output:
               Region identifier (str)
        '''
        if protein == 'No sequence available':
            return('Reference protein has no sequence')
            
        for i in range(0, len(protein)-len(peptide)+1):
            if peptide == protein[i:i+len(peptide)]:
                if i <= 19:
                    return('N-terminal')
                if i >= (len(protein)-39):
                    return('C-terminal')
                else:
                    return('Central')
    			


        

