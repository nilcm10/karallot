'''
Created on 25 d’ag. 2018

@author: quim
'''
import sys, re, ssl, datetime, operator
try:
    import pandas as pd
except:
    print('''
    Could not import pandas module. Timport xlsxwritero install it:
        $ python3 -m pip install pandas
    For further information, check https://pypi.org/project/pandas/''')
try:
    import urllib.request, urllib.error
except:
    print('''
    Could not import urllib module. To install it:
        $ python3 -m pip install urllib
    For further information, check
    https://docs.python.org/3/library/urllib.request.html#module-urllib.request
    ''')
try:
    from bs4 import BeautifulSoup
except:
    print('''
    Could not import bs4 module. To install it:
        $ python3 -m pip install bs4
    For further information, check https://pypi.org/project/bs4/''')

try:
    from openpyxl import load_workbook
except:
    print('''
    Could not import openpyxl module. To install it:
        $ python3 -m pip install openpyxl
    For further information, check https://pypi.org/project/openpyxl/''')
try:
    from PyQt4 import QtGui, uic
except:
    print('''
    Could not import PyQt4 module.
    For further information, check https://pypi.org/project/PyQt4/''')

qtCreatorFile = "principal.ui" # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtGui.QMainWindow, Ui_MainWindow):
    
    fin_name = ""
    fout_name = ""
        
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.boto.clicked.connect(self.open_file)
        self.fitxer_sortida.textChanged.connect(self.change_fout)
        self.boto2.clicked.connect(self.processa)
        self.despProGrAcc.addItem("")
        for item in self.repo:
            self.despProGrAcc.addItem(item[0])
        self.despProGrAcc.activated.connect(self.consultaBBDD)
        self.despSequence.activated.connect(self.refreshColumns)
        self.despPGAcc.activated.connect(self.refreshColumns)
        self.textProGrAcc.textChanged.connect(self.changeTextProGrAcc)
        self.botCerca.clicked.connect(self.cerca)
    
    def refreshColumns(self):
        self.sequenceColum = self.despSequence.currentText()
        self.PGAccColum = self.despPGAcc.currentText()
        self.despSequence.setEnabled(True)
        self.despPGAcc.setEnabled(True)
        if self.sequenceColum != "" and self.PGAccColum != "":
            self.boto2.setEnabled(True)
        else:
            self.boto2.setEnabled(False)
    
        
    def changeTextProGrAcc(self):
        item = self.textProGrAcc.text()
        if item != "" :
            self.botCerca.setEnabled(True)
        else:
            self.botCerca.setEnabled(False)
        
    def cerca(self):
        item = self.textProGrAcc.text()
        if item != "":
            item = str(self.uid_to_loc(item))
            self.dades.setText(item + "\n\n" + self.dades.toPlainText())
        self.refreshCombo()
        
        
    def consultaBBDD(self):
        item = self.despProGrAcc.currentText()
        if item != "":
            item = str(self.uid_to_loc(item))
            self.dades.setText(item + "\n\n" + self.dades.toPlainText())
            

    def change_fout(self):
        self.fout_name = self.fitxer_sortida.text() +".xlsx"
    
    sequenceColum = "Sequence"
    PGAccColum = "Protein Group Accessions"

    
    def open_file(self):
        self.fin_name = QtGui.QFileDialog.getOpenFileName(self, 'Obre el fitxer')
        self.hora_inici_fi.setText("")
        self.dades.setText("")
        llarg = len(self.fin_name) - 5
        if self.fin_name[llarg:] == ".xlsx":
            self.fout_name = self.fin_name[:llarg]+'_out'+self.fin_name[llarg:]
            self.label_fin.setText(self.fin_name)
            self.fitxer_sortida.setText(self.fin_name[:llarg]+'_out')
            sheet = pd.ExcelFile(self.fin_name).parse()
            columns = sheet.columns.get_values()
            self.despSequence.clear()
            self.despPGAcc.clear()
            self.despSequence.addItem("")
            self.despPGAcc.addItem("")
            i = 1
            seqCol = 0
            pgaCol = 0
            for column in columns:
                self.despSequence.addItem(column)
                if self.sequenceColum == column:
                    seqCol = i
                self.despPGAcc.addItem(column)
                if self.PGAccColum == column:
                    pgaCol = i
                i = i+1
            self.despSequence.setCurrentIndex(seqCol)
            self.despPGAcc.setCurrentIndex(pgaCol)
            self.sequenceColum = self.despSequence.currentText()
            self.PGAccColum = self.despPGAcc.currentText()
            self.refreshColumns()
        else:
            self.label_fin.setText("ERROR: el fitxer ha de ser .xlsx")
            self.boto2.setEnabled(False)
        #print(self.fout_name)

    def processa(self):
        hora_inici = str(datetime.datetime.now())
        self.hora_inici_fi.setText("From " + hora_inici)
        sheet = pd.ExcelFile(self.fin_name).parse()

        #Estadística de seqüències        
        sequences = []
        llargades = []
        for acc in sheet[self.sequenceColum]:
            hiEs = False
            llargades.append(len(acc))
            for seq in sequences :
                if acc == seq[0]:
                    seq[2] = seq[2] + 1
                    hiEs = True
                     
            if hiEs == False:
                llargada = len(acc)
                nova = []
                nova = [acc, llargada, 1]
                
                sequences.append(nova)
                
                
        resumSequences = []
        for seq in sequences:
            hiEs = False
            for freq in resumSequences :
                if seq[2] == freq[0]:
                    freq[1] = freq[1] + 1
                    hiEs = True
                     
            if hiEs == False:
                nova = []
                nova = [seq[2], 1]
                resumSequences.append(nova)
            
        resumSequences = sorted(resumSequences, key=lambda resum: resum[0])
        
        
        totalSeq = 0
        totalPep = 0
        j = 0
        for i in resumSequences:
            peptides = i[0] * i[1]
            i.append(peptides)
            totalSeq = totalSeq + i[1]
            totalPep = totalPep + (i[0] * i[1])
            j = j + 1
        
        resumSequences = sorted(resumSequences, key=operator.itemgetter(0))
        resumSequences.append(["Total",totalSeq, totalPep])
        
        resumLlargades = []
        for ll in llargades:
            hiEs = False
            for rll in resumLlargades:
                if rll[0] == ll:
                    rll[1] = rll[1] + 1
                    hiEs = True
            if hiEs == False:
                nova = []
                nova = [ll, 1]
                resumLlargades.append(nova)
        jj = 0
        for rll in resumLlargades:
            pes = rll[1]/totalPep
            jj = jj + 1
            rll.append(pes)
        
        resumLlargades = sorted(resumLlargades, key=operator.itemgetter(0))
        resumLlargades.append(["Total",totalPep, 1])
        
        
        
        # Appending new columns to sheet
        loc_ids, loc_names, z = [],[],0
        for acc in sheet[self.PGAccColum]:
            if ';' in acc:
                cut = acc.index(';')
                acc = acc[:cut]
            pair = self.uid_to_loc(acc)  #Era from_uid_to_loc

            z += 1
            
            sys.stdout.write('%s / %s \r' % (z, len(sheet))) # progress
            sys.stdout.flush()
            
            self.lbComptador.setText('%s / %s \r' % (z, len(sheet)))
            progres = (z / len(sheet)) * 100
            self.barraProgres.setValue(progres)
            QtGui.qApp.processEvents()
                
            loc_names.append(pair[1]) # location ID
            loc_ids.append(pair[2]) # location name

        # Appending new columns to sheet
        loc_col = [str(x)[1:-1] for x in loc_ids]
        
        loc_names = [str(x)[1:-1] for x in loc_names]
        
        llargada = [str(x) for x in llargades]

        resumLocNames = []
        hiEs = False
        for loc_id in loc_ids:
            #print(loc_id)
            hiEs = False
            for loc_idR in resumLocNames:
                if loc_idR[0] == loc_id:
                    hiEs = True
                    loc_idR[1] = loc_idR[1] + 1
            if hiEs == False:
                nova = []
                nova = [loc_id, 1]
                resumLocNames.append(nova)
                
        jjj = 0
        for loc_idR in resumLocNames:
            pes = loc_idR[1]/totalPep
            jjj = jjj + 1
            loc_name = []
            for idr in loc_idR[0]:
                loc_name.append(self.terms[idr])
            loc_idR.append(loc_name)
            loc_idR.append(pes)
                
        
        #resumLocNames = sorted(resumLocNames, key=operator.itemgetter(1), reverse = True)
        
        resumLocNamesRR = []
        for rl in resumLocNames:
            resumLocNamesR = []
            resumLocNamesR.append(rl[0])
            resumLocNamesR.append(rl[2])
            resumLocNamesR.append(rl[1])
            resumLocNamesR.append(rl[3])
            resumLocNamesRR.append(resumLocNamesR)

        resumLocNames = resumLocNamesRR
        
        resumLocNames = sorted(resumLocNames, key=lambda resumLocNames:[-resumLocNames[2], resumLocNames[1]])
        
        resumLocNames.append(["Total","",totalPep, 1])

        sheet['Length'] = pd.Series(llargada, index=sheet.index)
        sheet['Location ID'] = pd.Series(loc_col, index=sheet.index)
        sheet['Location Names'] = pd.Series(loc_names, index=sheet.index)
        
        
        # Writing out excel file
        fout = pd.ExcelWriter(self.fout_name, engine='xlsxwriter')
        sheet.to_excel(fout,'Base')
        
        pdResumSequences = pd.DataFrame(resumSequences,columns=['Freq.', 'Sequences', 'Peptides'])
        pdResumSequences.to_excel(fout, sheet_name='EstatSequences')

        #fout.save()

        workbook  = fout.book
        format1 = workbook.add_format({'num_format': '#,##0'})
        format2 = workbook.add_format({'num_format': '0.00%'})

        worksheet = fout.sheets['EstatSequences']
        worksheet.set_column('D:D', None, format1)
        worksheet.set_column('C:C', None, format1)

        # Create a chart object.
        chart = workbook.add_chart({'type': 'column'})

        # Configure the series of the chart from the dataframe data.
        rangCat = '=EstatSequences!$C$1:$D$1'
        rangDades = '=EstatSequences!$C$' + str(j+1)+ ':$D$' + str(j+1)
        
        chart.add_series({
            'categories': rangCat,
            'values': rangDades,
            'gap': 2,
        })
        
        chart.set_y_axis({'major_gridlines': {'visible': False}})
        chart.set_legend({'position': 'none'})
        chart.set_title({'name': 'Sequences frequencies',
                         'name_font': {'name': 'Calibri',
                                       'color': 'blue',},
                         })
        
        # Insert the chart into the worksheet.
        worksheet.insert_chart('F1', chart)
        
        
        pdResumLlargades = pd.DataFrame(resumLlargades,columns=['Length', 'Compta - Length', '% - Length'])
        pdResumLlargades.to_excel(fout, sheet_name='EstatLength')
        
        #workbook  = fout.book
        worksheet = fout.sheets['EstatLength']
        worksheet.set_column('B:B', None, format1)
        worksheet.set_column('C:C', None, format1)
        worksheet.set_column('D:D', None, format2)

        # Create a chart object.
        chart = workbook.add_chart({'type': 'column'})
        chartPie = workbook.add_chart({'type': 'pie'})

    
        # Configure the series of the chart from the dataframe data.
        rangCat = '=EstatLength!$B$2:$B$' + str(jj+1)
        rangDades = '=EstatLength!$C$2:$C$' + str(jj+1)
        
        chart.add_series({
            'categories': rangCat,
            'data_labels': {'value': True},
            'values': rangDades,
            'gap': 2,
        })
        chartPie.add_series({
            'categories': rangCat,
            'data_labels': {'position': 'outside_end',
                            'percentage': True},
            'values': rangDades
        })        
        chart.set_y_axis({'major_gridlines': {'visible': True}})
        chart.set_legend({'position': 'none'})
        chart.set_title({'name': 'All length Distributions MIS>=35',
                         'name_font': {'name': 'Calibri',
                                       'color': 'blue',},
                         })
        chartPie.set_title({'name': 'All length Distributions %',
                         'name_font': {'name': 'Calibri',
                                       'color': 'blue',},
                         })
        # Insert the chart into the worksheet.
        worksheet.insert_chart('F1', chart)
        worksheet.insert_chart('F17', chartPie)

        pdResumLocNames = pd.DataFrame(resumLocNames,columns=['LocID','LocNames', 'Compta - LocNames', '% - LocNames'])
        pdResumLocNames.to_excel(fout, sheet_name='EstatLocNames')
        
        #workbook  = fout.book
        worksheet = fout.sheets['EstatLocNames']
        
        worksheet.set_column('D:D', None, format1)
        worksheet.set_column('E:E', None, format2)
        # Create a chart object.
        chart = workbook.add_chart({'type': 'column'})
        chartPie = workbook.add_chart({'type': 'pie'})

    
        # Configure the series of the chart from the dataframe data.
        rangCat = '=EstatLocNames!$B$2:$B$' + str(jjj+1)
        rangDades = '=EstatLocNames!$D$2:$D$' + str(jjj+1)
        
        chart.add_series({
            'categories': rangCat,
            'data_labels': {'value': True},
            'values': rangDades,
            'gap': 2,
        })
        chartPie.add_series({
            'categories': rangCat,
            'data_labels': {'position': 'outside_end',
                            'percentage': True},
            'values': rangDades
        })        
        chart.set_y_axis({'major_gridlines': {'visible': True}})
        chart.set_legend({'position': 'none'})
        chart.set_title({'name': 'All LocNames Distributions',
                         'name_font': {'name': 'Calibri',
                                       'color': 'blue',},
                         })
        chartPie.set_title({'name': 'All LocNames Distributions %',
                         'name_font': {'name': 'Calibri',
                                       'color': 'blue',},
                         })
        # Insert the chart into the worksheet.
        worksheet.insert_chart('G1', chart)
        worksheet.insert_chart('G17', chartPie)


        
        fout.save()
        
        
        # Done!
        hora_fi = str(datetime.datetime.now())
        self.hora_inici_fi.setText("From " + hora_inici + " to " + hora_fi)
        self.lbComptador.setText('%s / %s Done!!' % (z, len(sheet)))
        self.refreshCombo()
        
    def refreshCombo(self):
        self.despProGrAcc.clear()
           
        self.despProGrAcc.addItem("")
        for item in self.repo:
            self.despProGrAcc.addItem(item[0])
        QtGui.qApp.processEvents()

    # Dictionary with the location names and ids correspondance
    terms = {1:['Extracellular region', 'Secreted'],
             2: ['Endosome', 'Lysosome'],
             3: ['Plasma membrane', 'Membrane', 'Cell membrane'],
             4: ['Golgi apparatus'],
             5: ['Endoplasmic reticulum', 'Ribosomal'],
             6: ['Mitochondrion'],
             7: ['Cytosol', 'Cytoplasm', 'Cytoskeleton'],
             8: ['Nucleus'],
             9: ['Other locations'],
             0: ['Undefined']}

    # Defining functions
    def loc_id(self, locs, uid):
        '''
        From a location name to a location ID. Looks for the correspondance in the
        dictionary terms, and returns the location ID.
    
        Input:
            List of location names
        Expected output:
            List of location IDs
        '''
        if locs[0][:5] != 'Error':
            lenlocs = len(locs)
            ids = [None]*lenlocs


            if lenlocs < 1:
                ids = [0]
                print('''
            >>> No subcellular location is defined for {}. Check GO Annotation.
            '''.format(uid))
            else:
                for i in range(lenlocs):
                    for j in range(len(self.terms)):
                        if locs[i] in self.terms[j]:
                            ids[i] = j
       
            for i in range(len(ids)):
                if ids[i] == None:
                    #other_locs.append(locs[i])
                    ids[i] = 9
                    
            ids = list(set(ids))
            ids.sort()
            return ids

    #Llegim repositori local de Locations ID
    sheetRepo = pd.ExcelFile('LocationIDRepo.xlsx').parse()
    repoUID = []
    repoLocID = []
    repo = []
    
    for i in sheetRepo['UID']:
        repoUID.append(i)
        
    for i in sheetRepo['LocID']:
        repoLocID.append(i)
        
    for i in range(len(repoUID)):
        repoRow= []
        repoRow.append(repoUID[i])
        repoRow.append(eval(repoLocID[i]))
        repo.append(repoRow)
    repo = sorted(repo, key=operator.itemgetter(0))
    

    def uid_to_loc(self, uid):
        uidLoc = []
        hiEs = False
        for uidR in self.repo:
            hiEs = False
            if uidR[0] == uid:
                hiEs = True
                term = []
                for u in uidR[1]:
                    #print(u)
                    term.append(self.terms[u])
                uidLoc = [uidR[0],term,uidR[1]]
                break
        #Si no hi es busquem a internet i l'afegim al Repo Local
        if hiEs == False:
            uidLoc = self.from_uid_to_loc(uid)
            row_data = [uidLoc[0],uidLoc[2]]
            row_dataStr = [uidLoc[0],str(uidLoc[2])]
            #print(row_dataStr)
            if uidLoc[1][0][:5] != 'Error':
                self.repo.append(row_data)
            
                wb = load_workbook("LocationIDRepo.xlsx")
                # Select First Worksheet
                ws = wb.worksheets[0]
                ws.append(row_dataStr)
                wb.save("LocationIDRepo.xlsx")
                self.repo = sorted(self.repo, key=operator.itemgetter(0))
        return uidLoc
    
    
    
    def from_uid_to_loc(self, uid):
        '''
        From a UniProt ID to a location. The function parses each UniProt ID entry
        to find the location.
    
        Input:
            UniProt ID (str)
        Expected output:
            Tuple with the UniProt ID, the location name and ID.
        '''

        # to avoid SSL certificate errors (from @csev)
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        
        URL = 'https://www.uniprot.org/uniprot/' + uid
        locs = []
        try:
            u = urllib.request.urlopen(URL, context=ctx).read()
            uBS = BeautifulSoup(u, 'html.parser')
            str2='Keywords - Cellular component'
            uu=u.decode("utf-8") 
            #print(URL)
            #div = uBS('div')
            hies=uu.find(str2)
            if hies!=-1:
                s=uBS.find(id="subcellular_location")
                if s != []:
                    span=s('span')
                    for j in range(len(span)):
                        p = re.findall('.*(Cellular component).*', str(span[j]))
                        if p != []:
                            loc = span[j+1]
                            for l in loc('a'):
                                #print(l)
                                kw = re.findall('>(.*?)</a>', str(l))
                                if len(kw) > 0:
                                    locs.append(kw[0])
                                
        except urllib.error.HTTPError as e:
            if e.code == 404:
                locs=['Error 404: Not found']
            else:
                locs=["Error " + u.code + ': ']
        except urllib.error.URLError as e:
            locs=["Error :" + e.reason]


        if locs == []:
            locs=['Undefined']
        
        
        return (uid, locs, self.loc_id(locs, uid))

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
